import os, json, shutil
import vtk, qt, slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin

class CheckDetections(ScriptedLoadableModule):
    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = "Check Detections"
        self.parent.categories = ["Aneurysms"]
        self.parent.dependencies = []
        self.parent.contributors = ["Youssef Assis (Loria lab.)"]
        self.parent.acknowledgementText = "This module was developed by Youssef Assis"

class CheckDetectionsWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
    def __init__(self, parent=None):
        ScriptedLoadableModuleWidget.__init__(self, parent)
        VTKObservationMixin.__init__(self)
        slicer.app.pythonConsole().clear()

    def setup(self):
        ScriptedLoadableModuleWidget.setup(self)
        uiWidget = slicer.util.loadUI(self.resourcePath('UI/CheckDetections.ui'))
        self.layout.addWidget(uiWidget)
        self.ui = slicer.util.childWidgetVariables(uiWidget)
        uiWidget.setMRMLScene(slicer.mrmlScene)

        # Volume
        self.ui.vol_visibility.stateChanged.connect(self.set_volume_visibility)
        self.ui.volumeThreshold.valueChanged.connect(self.onVolumeThresholdChanged)
        self.ui.volumeOpacity.valueChanged.connect(self.onVolumeOpacityChanged)
        self.ui.volumeColor.colorChanged.connect(self.set_volume_color)

        self.ui.detectionsOpacity.valueChanged.connect(self.set_spheres_opacity)
        self.ui.SpheresColorPicker.colorChanged.connect(self.set_spheres_color)

        self.ui.confidence_checkBox.stateChanged.connect(self.confidence_visibility)
        self.ui.cropBox.stateChanged.connect(self.onCropChanged)
        self.ui.detectionsVisibility.stateChanged.connect(self.set_detections_visibility)

        self.ui.load_button.connect('clicked(bool)', self.load_files)
        self.ui.saveButton.connect('clicked(bool)', self.SaveButton)

        self.initializeParameterNode()


    # Manage Volume
    def set_volume_visibility(self, value=None):
        value = self.ui.vol_visibility.checked if value is None else value
        if self.volNode:
            displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
            if displayNode is not None:
                displayNode.SetVisibility(value)

    def onVolumeThresholdChanged(self, value):
        if self.volNode:
            displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
            volumePropertyNode = displayNode.GetVolumePropertyNode()

            # get the scalar opacity transfer function
            scalarOpacity = volumePropertyNode.GetVolumeProperty().GetScalarOpacity()

            # get the range of the scalar volume
            rang = self.volNode.GetImageData().GetScalarRange()
            
            # set the threshold
            margin = rang[1]/10
            upperThreshold = value
            scalarOpacity.RemoveAllPoints()
            scalarOpacity.AddPoint(upperThreshold, self.ui.volumeOpacity.value)
            scalarOpacity.AddPoint(max(rang[0], upperThreshold-margin), 0.0)

    def onVolumeOpacityChanged(self):
        if self.volNode:
            displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
            scalarOpacity = displayNode.GetVolumePropertyNode().GetVolumeProperty().GetScalarOpacity()
            p = [0]*4
            scalarOpacity.GetNodeValue(1, p)
            p[1] = self.ui.volumeOpacity.value
            scalarOpacity.SetNodeValue(1, p)

    def set_volume_color(self, color=None):
        if self.volNode:
            color = [self.ui.volumeColor.color.red()/255, 
                     self.ui.volumeColor.color.green()/255, 
                     self.ui.volumeColor.color.blue()/255]
            displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
            rangeCT = [0] * 2
            displayNode.GetVolumePropertyNode().GetColor().GetRange(rangeCT)
            colorTransferFunction = vtk.vtkColorTransferFunction()
            colorTransferFunction.AddRGBPoint(rangeCT[0], color[0], color[1], color[2])
            colorTransferFunction.AddRGBPoint(rangeCT[1], color[0], color[1], color[2])
            displayNode.GetVolumePropertyNode().SetColor(colorTransferFunction)

    def ResetCamera(self):
        camera = slicer.util.getNode('Camera').GetCamera()
        camera.SetFocalPoint([0,0,0])
        camera.SetPosition([0, 500, 0])
        camera.SetViewUp([0, 0, 1])

    def initializeParameterNode(self):
        # Volume
        self.volNode = None
        self.ui.Volume_CollapsibleGroupBox.enabled = 0
        self.ui.vol_visibility.checked = 1

        # Detections
        self.ui.Detections_CollapsibleGroupBox.enabled = 0
        self.ui.confidence_checkBox.checked = 0
        self.ui.saveButton.enabled = 0
        self.ui.cropBox.checked = 0

        self.output_annotations = None
        self.ui.CollapsibleButton.text = "Outputs"

        self.out_file = None
        self.file_saved = True

        self.selected_detection_id = None

        # comment field
        self.commentField = qt.QPlainTextEdit()
        self.commentField.setFixedHeight(30)
        self.commentField.placeholderText = "Here you can add some comments"


        # table initialization
        columns = ['Confidence', 'Radius', 'Aneurysm ?', 'Doubt ?', 'Location', 'Morphology']
        self.tableWidget = qt.QTableWidget()

        # self.tableWidget.setMaximumHeight(150)
        self.tableWidget.setColumnCount(len(columns))
        self.tableWidget.setHorizontalHeaderLabels(columns)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setVisible(False)

        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.resizeRowsToContents()
        self.tableWidget.setSelectionBehavior(self.tableWidget.SelectRows)

        self.tableWidget.setColumnWidth(0, 70)
        self.tableWidget.setColumnWidth(1, 48)
        self.tableWidget.setColumnWidth(2, 78)
        self.tableWidget.setColumnWidth(3, 55)
        self.tableWidget.setColumnWidth(4, 130)

        self.locations = ["-", "Acom", "Pericallosal", "A1", "MCA division", "M1", "Distal MCA", "Carotid T°", "AChA", "Pcom", "Carotid oph", "Carotid cave", "Para oph", "Cavernous", "Basilar tip", "PICA", "PCA", "SCA", "Other basilar/V4", "* Vein", "* External carotid", "* Extravascular"]
        self.morphology = ["-", "* Infundibulum", "* Fusiform", "* Dysplasia", "* Hemo/sequela", "Perforator's origin", "Bend/loop", "Atheroma", "Post-coiling", "Motion artifacts", "Noise"]
        self.layout_2 = qt.QGridLayout()
        self.layout_2.addWidget(self.tableWidget)
        self.layout_2.addWidget(self.commentField)
        self.ui.CollapsibleButton.setLayout(self.layout_2)

        slicer.util.findChild(slicer.util.mainWindow(), "DataProbeCollapsibleWidget").setVisible(False)    

    def load_files(self):
        if not self.file_saved and slicer.util.confirmYesNoDisplay("Do you want to save annotations before loading new data ?", "Confirmation"):
            self.SaveButton()

        self.commentField.plainText = ""
        dialog = qt.QFileDialog()
        dir_path = str(dialog.getExistingDirectory())
        if dir_path:
            # load the first volume file
            nii_volume_files = [f for f in os.listdir(dir_path) if f.endswith(".nii.gz")]
            if nii_volume_files:
                if len(nii_volume_files)>1: slicer.util.errorDisplay('There too many volume files in that folder. Loading the first one.')
                self.remove_existing_volumes()
                slicer.util.loadVolume(os.path.join(dir_path, nii_volume_files[0]))

                self.volNode = slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")[0]
                # Add volume to the 3D view
                volRenLogic = slicer.modules.volumerendering.logic()
                displayNode = volRenLogic.GetFirstVolumeRenderingDisplayNode(self.volNode)
                if not displayNode: displayNode = volRenLogic.CreateDefaultVolumeRenderingNodes(self.volNode)
                volRenLogic.UpdateDisplayNodeFromVolumeNode(displayNode, self.volNode)

                # Set the thresholding value 
                min_val, max_val = self.volNode.GetImageData().GetScalarRange()
                self.ui.volumeThreshold.maximum = max_val
                self.ui.volumeThreshold.minimum = min_val
                self.ui.volumeThreshold.value = (max_val+min_val)/2

                self.ui.Volume_CollapsibleGroupBox.enabled = 1
                self.ui.vol_visibility.checked = 1

            # loading detection file = json file
            json_files = [f for f in os.listdir(dir_path) if f.endswith(".json")]
            if json_files:
                file_output = [f for f in json_files if "_output.json" in f]
                file = file_output[0] if file_output else json_files[0]
                self.out_file = os.path.join(dir_path, file) if "_output.json" in file  else os.path.join(dir_path, file).replace(".json", "_output.json")

                self.delete_model_by_names(["Confidences", "Annotations"])
                self.file_saved = False

                # delete existing detections
                self.ui.Detections_CollapsibleGroupBox.enabled = 1
                self.ui.CollapsibleButton.collapsed = True
                self.selected_detection_id = None
                self.ui.detectionsVisibility.checked = 0
                self.ui.confidence_checkBox.checked = 0
                self.ui.cropBox.checked = 0
                self.ui.saveButton.enabled = 1

                self.onCropChanged()
                self.load_detections_from_file(os.path.join(dir_path, file))
                self.set_spheres_color()
                self.set_spheres_opacity()

            comment_file = [f for f in os.listdir(dir_path) if f.endswith(".txt")]
            if comment_file:
                file = comment_file[0]
                f = open(os.path.join(dir_path, file), "r")
                txt = f.read()
                self.commentField.plainText = txt

    def show_detections_info(self):
        self.tableWidget.setRowCount(len(self.output_annotations))               
        # self.tableWidget.setMaximumHeight(len(self.output_annotations) * 45)

        for idx, (sphere_id, annot) in enumerate(self.output_annotations.items()):  
            # Confidence
            confidence = str(round(annot['confidence']*100, 2))
            itm = qt.QTableWidgetItem(confidence)
            itm.setTextAlignment(4)
            self.tableWidget.setItem(idx, 0,  itm)

            # Radius
            radius = str(round(annot['radius'], 3))
            radius_item = qt.QTableWidgetItem(radius)
            radius_item.setTextAlignment(4)
            self.tableWidget.setItem(idx, 1,  radius_item)

            # Aneurysm ?
            cbox = qt.QCheckBox()
            cbox.checked = int(annot['truth'])
            cell_widget = qt.QWidget()
            lay_out = qt.QHBoxLayout(cell_widget)
            lay_out.addWidget(cbox)
            lay_out.setAlignment(qt.Qt.AlignCenter)
            lay_out.setContentsMargins(0,0,0,0)
            cell_widget.setLayout(lay_out)
            self.tableWidget.setCellWidget(idx, 2, cell_widget)
            cbox.clicked.connect(lambda checked, id=int(sphere_id): self.onAneurysmStateChanged(checked, id))

            # Doubt ?
            cbox = qt.QCheckBox()
            cbox.checked = int(annot['doubt'])
            cell_widget = qt.QWidget() # create widget in order to center the CheckBoxe in the middle of the cell
            lay_out = qt.QHBoxLayout(cell_widget)
            lay_out.addWidget(cbox)
            lay_out.setAlignment(qt.Qt.AlignCenter)
            lay_out.setContentsMargins(0,0,0,0)
            cell_widget.setLayout(lay_out)
            self.tableWidget.setCellWidget(idx, 3, cell_widget)
            cbox.clicked.connect(lambda checked, id=int(sphere_id): self.onDoubtStateChanged(checked, id))

            # Locations
            item_index = 0
            lv = qt.QComboBox()
            for i, l in enumerate(self.locations):
                lv.addItem(l)
                if item_index == 0 and l == annot['location']:
                    item_index = i
            lv.setCurrentIndex(item_index)
            self.tableWidget.setCellWidget(idx, 4, lv)
            lv.currentIndexChanged.connect(lambda index, id=int(sphere_id): self.set_location(index, id))

            # Morphology
            item_index = 0
            lv = qt.QComboBox()
            for i, l in enumerate(self.morphology):
                lv.addItem(l)
                if item_index == 0 and l == annot['morphology']:
                    item_index = i
            lv.setCurrentIndex(item_index)
            self.tableWidget.setCellWidget(idx, 5, lv)
            lv.currentIndexChanged.connect(lambda index, id=int(sphere_id): self.set_morphology(index, id))

            # Event: on row selected then display associated/selected detection
            self.tableWidget.clicked.connect(self.onRowSelected)

    def set_spheres_color(self):
        color = (self.ui.SpheresColorPicker.color.red()/255, 
                 self.ui.SpheresColorPicker.color.green()/255, 
                 self.ui.SpheresColorPicker.color.blue()/255)
        nodes = slicer.mrmlScene.GetNodesByName("Annotations")
        for node in nodes:
            node.GetDisplayNode().SetColor(color)

    def set_spheres_opacity(self):
        # get model by name + update opacity
        nodes = slicer.mrmlScene.GetNodesByName("Annotations")
        for node in nodes:
            node.GetDisplayNode().SetOpacity(self.ui.detectionsOpacity.value/2)

    def confidence_visibility(self):
        for node in slicer.mrmlScene.GetNodesByName("Confidences"):
            node.GetDisplayNode().SetVisibility(self.ui.confidence_checkBox.checked)
        if self.selected_detection_id is not None and self.ui.confidence_checkBox.checked:
            node = slicer.mrmlScene.GetFirstNodeByName("Confidences")
            for idx in range(0, node.GetNumberOfControlPoints()):
                if idx != self.selected_detection_id:
                    node.SetNthControlPointVisibility (idx, False)
                else:
                    node.SetNthControlPointVisibility (idx, True)

    def set_detections_visibility(self, value):
        if self.selected_detection_id is not None:
            nodes = slicer.mrmlScene.GetNodesByName("Annotations")
            nodes.GetItemAsObject(self.selected_detection_id).GetDisplayNode().SetVisibility(self.ui.detectionsVisibility.checked)
        else:
            for node in slicer.mrmlScene.GetNodesByName("Annotations"):
                node.GetDisplayNode().SetVisibility(self.ui.detectionsVisibility.checked)


    def onRowSelected(self, item):
        # detection visibility
        self.selected_detection_id  = item.row() if not isinstance(item, int) else item
        self.confidence_visibility()

        for idx in range(0, slicer.mrmlScene.GetNodesByName("Annotations").GetNumberOfItems()):
            node = slicer.mrmlScene.GetNodesByName("Annotations").GetItemAsObject(idx)

            if idx == self.selected_detection_id:
                node.GetDisplayNode().SetVisibility(self.ui.detectionsVisibility.checked)
                position_RAS = list(self.output_annotations.values())[idx]['center']
                slicer.vtkMRMLSliceNode.JumpAllSlices(slicer.mrmlScene, *position_RAS, slicer.vtkMRMLSliceNode.CenteredJumpSlice)
            else:
                node.GetDisplayNode().SetVisibility(False)

        self.ui.detectionsVisibility.checked = 1
        
        # activate crop ?
        self.onCropChanged()

        # camera focal point 
        threeDViewWidget = slicer.app.layoutManager().threeDWidget(0)
        cameraDisplayableManager = threeDViewWidget.threeDView().displayableManagerByClassName('vtkMRMLCameraDisplayableManager')
        camera = cameraDisplayableManager.GetCameraWidget().GetCameraNode()
        position_RAS = list(self.output_annotations.values())[self.selected_detection_id]['center']
        camera.SetFocalPoint(position_RAS)

    def onCropChanged(self):
        isCrop = self.ui.cropBox.checked

        roiNode = slicer.util.getNodesByClass("vtkMRMLMarkupsROINode")
        if len(roiNode) > 0:
            roiNode[0].GetDisplayNode().SetVisibility(isCrop)

        if isCrop:
            if len(roiNode) == 0:
                roiNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLMarkupsROINode") 
                roiNode.SetName("ROI")  
                roiNode.GetDisplayNode().SetOpacity(0.05)
                roiNode.GetDisplayNode().SetVisibility(isCrop)
            else:
                roiNode = roiNode[0]
            
            if self.selected_detection_id is not None:
                position_RAS = list(self.output_annotations.values())[self.selected_detection_id]['center']
                radius = [list(self.output_annotations.values())[self.selected_detection_id]['radius']*4 ] * 3
                roiNode.SetCenter(position_RAS)
                roiNode.SetRadiusXYZ(radius)

            if self.volNode:
                displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
                if displayNode:
                    displayNode.SetCroppingEnabled(True)
                    displayNode.SetAndObserveROINodeID(roiNode.GetID())
                    displayNode.GetROINode().SetDisplayVisibility(True)

        if not isCrop and self.volNode:
            displayNode = slicer.modules.volumerendering.logic().GetFirstVolumeRenderingDisplayNode(self.volNode)
            if displayNode: displayNode.SetCroppingEnabled(False)

    def set_location(self, index, detection_id):
        # print(f"Selected Morphology: {self.locations[index]} for detection {detection_id}")
        self.onRowSelected(detection_id)
        if self.output_annotations is not None:
            self.output_annotations[str(detection_id)]["location"] = self.locations[index]
            self.file_saved = False

    def set_morphology(self, index, detection_id):
        # print(f"Selected Morphology: {self.morphology[index]} for detection {detection_id}")
        self.onRowSelected(detection_id)
        if self.output_annotations is not None:
            self.output_annotations[str(detection_id)]["morphology"] = self.morphology[index]
            self.file_saved = False

    def onAneurysmStateChanged(self, checked, detection_id):
        self.onRowSelected(detection_id)
        if self.output_annotations is not None:
            self.output_annotations[str(detection_id)]["truth"] = int(checked)
            self.file_saved = False

    def onDoubtStateChanged(self, checked, detection_id):
        self.onRowSelected(detection_id)
        if self.output_annotations is not None:
            self.output_annotations[str(detection_id)]["doubt"] = int(checked)
            self.file_saved = False

    def onThresholdValuesChanged(self, lower, upper):
        if self.volNode is not None:
            displayNode = self.volNode.GetDisplayNode()
            displayNode.SetThreshold(lower, upper)
            displayNode.ApplyThresholdOn()

    def SaveButton(self):
        if self.output_annotations is None or self.out_file is None:
            slicer.util.errorDisplay(f"No detection file found")
        else:
            if os.path.isfile(self.out_file) and not slicer.util.confirmYesNoDisplay("Do you want to overwrite the existing files?", "Confirmation"):
                    return

            save_json(self.output_annotations, self.out_file)
            self.file_saved = True

            if self.commentField.plainText != "":
                out_description = os.path.join(os.path.dirname(self.out_file), "description.txt")

                with open(out_description, 'w', encoding='utf-8') as f:
                    f.write(self.commentField.plainText)

            if "Done" not in self.out_file:
                out_directory = os.path.join(os.path.dirname(os.path.dirname(self.out_file)), "Done")
                if not os.path.exists(out_directory):
                    os.makedirs(out_directory)
                # Move directory to new path
                shutil.move(os.path.dirname(self.out_file), out_directory)

                path_to_file, file_name = os.path.split(self.out_file)
                self.out_file = os.path.join(out_directory, path_to_file.split('/')[-1], file_name)

    def cleanup(self):
        slicer.mrmlScene.Clear(0)
        self.removeObservers()

    def load_detections_from_file(self, spheres_path):
        try:
          with open(spheres_path, 'r') as f:
              data = json.load(f)
        except:
          slicer.util.errorDisplay('Please choose the annotation file with the right format.')
          return

        # sort detectitons by confidence score + apply threshold=0.5
        self.output_annotations = {k: data[k] for k in sorted(data, key=lambda x:data[x]['confidence'], reverse=True) if data[k]['confidence']>=0.5}
        first_time = len([f for f in self.output_annotations.values() if 'truth' in f])==0 # else loading exisitng checked annotations

        # Centers = confidence scores
        pointListNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLMarkupsFiducialNode")
        pointListNode.SetName("Confidences")
        pointListNode.GetDisplayNode().SetSelectedColor(0, 1, 0)
        pointListNode.GetDisplayNode().SetColor(0, 1, 0)
        pointListNode.GetDisplayNode().SetVisibility(self.ui.confidence_checkBox.checked)
        for i, (idx, sphere) in enumerate(self.output_annotations.items()):
            n = pointListNode.AddControlPoint(sphere['center'])
            pointListNode.SetNthControlPointLabel(n, str(round(sphere['confidence'] * 100, 2))+" %")
            pointListNode.SetLocked(1)
            
            if first_time:
                self.output_annotations[str(idx)]["truth"] = 0
                self.output_annotations[str(idx)]["doubt"] = 0
                self.output_annotations[str(idx)]["location"] = "-"
                self.output_annotations[str(idx)]["morphology"] = "-"

        # Draw detections
        self.ui.CollapsibleButton.text = f'Outputs ({len(self.output_annotations)} detections)'
        self.draw_spheres()
        self.show_detections_info()

    def draw_spheres(self):
        for idx, (sphere_id, annot) in enumerate(self.output_annotations.items()):  
            center, radius = annot['center'], annot['radius']
            sphere = vtk.vtkSphereSource()

            sphere.SetCenter(center)
            sphere.SetRadius(radius)
            sphere.SetPhiResolution(30)
            sphere.SetThetaResolution(30)
            sphere.Update()

            model = slicer.modules.models.logic().AddModel(sphere.GetOutput())
            model.SetName("Annotations")
            model.GetDisplayNode().SetVisibility2D(True)
            model.GetDisplayNode().SetSliceIntersectionThickness(2)
            model.GetDisplayNode().SetVisibility(False)
        slicer.util.forceRenderAllViews()

    def remove_existing_volumes(self):
        nodes = slicer.util.getNodesByClass("vtkMRMLMarkupsROINode")
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        volumeNodes = slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")
        for node in volumeNodes:
            slicer.mrmlScene.RemoveNode(node)
        self.ui.Volume_CollapsibleGroupBox.enabled = 0
        self.volNode = None
        self.ResetCamera()

    def delete_model_by_names(self, names):
        for name in names:
            nodes = slicer.mrmlScene.GetNodesByName(name)
            for node in nodes:
                slicer.mrmlScene.RemoveNode(node)

def save_json(data: tuple, path: str, indent: int = 4):
    if os.path.isdir(os.path.dirname(path)):
        with open(path, "w") as f:
            json.dump(data, f, indent=indent)
        slicer.util.infoDisplay(f"Successfully saved to '{path}'", windowTitle="Info")
    else:
        slicer.util.errorDisplay(f"File already saved {path}")
